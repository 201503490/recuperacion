
from django.contrib import admin
from django.urls import path
from django.contrib.auth import views
from django.contrib.auth import login
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.views import LoginView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('login/',LoginView.as_view(template_name='registration/login.html'), name="login")
]
