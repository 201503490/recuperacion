from django.contrib.auth.forms import Authentication
from django import forms
class LoginForm(AuthenticationForm):
	username=forms.CharField(widget=forms.TextInput(attrs={'class':'form-control'}))
	password=forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control'}))
